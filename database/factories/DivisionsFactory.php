<?php

namespace Database\Factories;

use App\Models\Divisions;
use App\Models\Teams;
use Illuminate\Database\Eloquent\Factories\Factory;

class DivisionsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Divisions::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'teams_id' => Teams::factory(),
            'division' => 'a',
            'score' => 0
        ];
    }
}
