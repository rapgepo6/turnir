<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{
    use HasFactory;

    public function Divisions()
    {
        return $this->hasOne(Divisions::class);
    }

    public function PlayOffs()
    {
        return $this->hasOne(PlayOffs::class);
    }
}
