<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlayOffs extends Model
{
    use HasFactory;
    protected $fillable = ['team_id','turnir_id','place'];

    public function Teams()
    {
        return $this->belongsTo(Teams::class,'team_id');
    }
}
