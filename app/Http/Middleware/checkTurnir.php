<?php

namespace App\Http\Middleware;

use App\Models\Turnir;
use Closure;
use Illuminate\Http\Request;

class checkTurnir
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $query = Turnir::where('status','LIKE','active')->get();
        if ($query->count()) {
            $turnir = $query->first();
            $request->turnir_id = $turnir->id;
        } else {
            $turnir = Turnir::create(['status' => 'active']);
            $request->turnir_id = $turnir->id;
        }

        return $next($request);
    }
}
