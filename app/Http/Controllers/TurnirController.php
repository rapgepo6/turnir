<?php

namespace App\Http\Controllers;

use App\Models\Turnir;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class TurnirController extends Controller
{

    public function show(Request $request)
    {
        $divisionsA = [];
        $divisionsB = [];
        $playOffGrid = [];
        $playOffResults = [];


        if ($turnirId = $request->turnir_id) {
            $divisions = new DivisionsController();
            $divisionsA = $divisions->getDivisions('A',$turnirId);
            $divisionsB = $divisions->getDivisions('B',$turnirId);

            $playOff = new PlayOffsController();
            $playOffGrid = $playOff->getPlayOffGrid($turnirId);
        }

        return Inertia::render('Turnir',[
            'divisionsA' => $divisionsA,
            'divisionsB' => $divisionsB,
            'final' => $playOffGrid,
            'turnirId' => $turnirId,
            'playOffResults' => isset($playOffGrid['results']) ? $playOffGrid['results'] : [],
        ]);
    }

    public function generateTurnir(Request $request) {
        $turnirId = $request->turnir_id;
        $this->updateTurnir($turnirId);
        $turnir = Turnir::create(['status' => 'active']);
        return $turnir->id;
    }

    private function updateTurnir($id)
    {
        DB::table('turnirs')->where('id',$id)->update(['status' => 'done']);
    }

}
