<?php

namespace App\Http\Controllers;

use App\Models\Divisions;
use App\Models\Games;
use App\Models\PlayOffs;
use App\Models\Teams;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PlayOffsController extends Controller
{
    /**
     * Возврщаем массив разбитый по этапам
     * @return array
     */
    public function getPlayOffGrid($turnirId)
    {
        $teams = Teams::where('playoff','=',1)
            ->where('turnir_id','=',$turnirId)
            ->get()->toArray();

        $teamsId = array_column($teams,'id');

        if ($teamsId) {
            $games = Games::where('type', 'LIKE', 'playoff')
                ->where(function ($query) use ($teamsId) {
                    $query->whereIn('team_1', $teamsId);
                    $query->orWhereIn('team_2', $teamsId);
                })
                ->get()
                ->toArray();
            $grid = $this->teamsSplit($teams,$games);
            $grid['results'] = $this->getPlayOffResults($turnirId);

            return $grid;
        }

        return [];
    }

    // Разбиваем команды по таблице результатов
    private function teamsSplit($teams,$games)
    {
        $final = [];

        foreach ($games as $game) {
            $teamKey1 = array_keys(array_column($teams,'id'),$game['team_1']);
            $teamKey2 = array_keys(array_column($teams,'id'),$game['team_2']);
            $team_1 = $teams[array_shift($teamKey1)];
            $team_2 = $teams[array_shift($teamKey2)];
            if ($team_1['final'] && $team_2['final']) {
                $final['final'] = [
                    'team_1' => $team_1['team_name'],
                    'team_2' => $team_2['team_name'],
                    'score' => $game['score_1'].':'.$game['score_2'],
                ];
            } else if ($team_1['losers'] && $team_2['losers']) {
                $final['losers'] = [
                    'team_1' => $team_1['team_name'],
                    'team_2' => $team_2['team_name'],
                    'score' => $game['score_1'].':'.$game['score_2']
                ];
            } else if ($team_1['semi_final'] && $team_2['semi_final']) {
                $final['semi_final'][] = [
                    'team_1' => $team_1['team_name'],
                    'team_2' => $team_2['team_name'],
                    'score' => $game['score_1'].':'.$game['score_2']
                ];
            } else {
                $final['teams'][] = [
                    'team_1' => $team_1['team_name'],
                    'team_2' => $team_2['team_name'],
                    'score' => $game['score_1'].':'.$game['score_2']
                ];
            }
        }

        return $final;
    }

    /**
     * Устанавливаем участников плэй-оффа
     */
    public function generatePlayOff($turnirId)
    {
        $groupA = Divisions::where('division','LIKE','A')
            ->where('turnir_id','=',$turnirId)
            ->orderBy('score','desc')
            ->limit(4)
            ->get()
            ->toArray();
        $groupB = Divisions::where('division','LIKE','B')
            ->where('turnir_id','=',$turnirId)
            ->orderBy('score','desc')
            ->limit(4)
            ->get()
            ->toArray();

        $teamsId = array_merge(array_column($groupA,'teams_id'),array_column($groupB,'teams_id'));

        DB::table('teams')
            ->whereIn('id',$teamsId)
            ->update(['playoff' => 1]);
    }

    public function setPlayOffPlace($team,$place,$turnirId)
    {
        PlayOffs::create([
            'team_id' => array_shift($team),
            'turnir_id' => $turnirId,
            'place' => $place
        ]);
    }

    public function getPlayOffResults($turnirId)
    {
        return PlayOffs::with('Teams')
            ->where('turnir_id',$turnirId)
            ->get()
            ->toArray();
    }

}
