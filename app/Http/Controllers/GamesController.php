<?php

namespace App\Http\Controllers;

use App\Http\Middleware\checkTurnir;
use App\Models\Divisions;
use App\Models\Games;
use App\Models\Teams;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GamesController extends Controller
{

    /**
     * Generation games and update score for division and return scores
     * @param Request $request
     * @return array
     */
    public function generationDivisionGames(Request $request)
    {
        $divisionType = $request->type;
        $turnirId = $request->turnir_id;

        $query = \App\Models\Divisions::where('division','LIKE',$divisionType)
            ->where('turnir_id','=',$turnirId)
            ->get();
        $divisions = $query->toArray();

        $game = [];
        $games = [];
        $teams_id = array_column($divisions,'teams_id');

        // Генерируем счёт для каждой команды
        foreach ($divisions as $team) {
            foreach ($teams_id as $team_id) {
                if ($team_id != $team['teams_id']) {
                    $game[$team['teams_id']][$team_id] = rand(0, 7);
                }
            }
        }

        //Определяем победителя
        foreach ($game as $team_id => $item) {
            foreach ($item as $rival_id => $score) {
                // Обновляем счёт у команды победителя
                switch ($game[$rival_id][$team_id] <=> $score) {
                    case 1:
                        $division = \App\Models\Divisions::where('teams_id',$team_id)
                            ->where('turnir_id','=',$turnirId)
                            ->first();
                        $division->score = $division->score+1;
                        $division->save();
                        break;
                    case 0:
                        $score++;
                        break;
                }
                $games[] = [
                    'team_1' => $team_id,
                    'team_2' => $rival_id,
                    'score_1' => $game[$rival_id][$team_id],
                    'score_2' => $score,
                    'turnir_id' => $turnirId,
                    'type' => 'division'
                ];
            }
        }

        \App\Models\Games::insert($games);

        return $this->getGameScores($request);
    }

    /**
     * Возвращаем сетку игр для дивизиона А или В
     * @param Request $request
     * @return array
     */
    public function getGameScores(Request $request)
    {
        $divisionType = $request->type;
        $turnirId = $request->turnir_id;
        $divisions = Divisions::with('Teams')
            ->where('division', 'LIKE', $divisionType)
            ->where('turnir_id', '=', $turnirId)
            ->get()
            ->toArray();

        $teams_id = array_column($divisions,'teams_id');

        $games = Games::whereIn('team_1',$teams_id)
            ->orWhereIn('team_2',$teams_id)
            ->where('turnir_id','=',$turnirId)
            ->get()
            ->toArray();

        $result = [];
        foreach ($divisions as $key => $division) {
            foreach ($games as $game) {
                if ($division['teams_id'] == $game['team_1']) {
                    $division['game'][$game['team_2']] = $game['score_1'].':'.$game['score_2'];
                } else if ($division['teams_id'] == $game['team_2']) {
                    $division['game'][$game['team_1']] = $game['score_2'].':'.$game['score_1'];
                }
            }
            $result[$key] = $division;
        }

        return $result;
    }

    /**
     * Генерируем игры, и определяем победителей
     *
     * @param Request $request
     * @return array - данные для frontend
     */
    public function generationPlayOffGames(Request $request)
    {
        $turnirId = $request->turnir_id;
        $playOff = new PlayOffsController();
        $playOff->generatePlayOff($turnirId);

        $teams = Teams::with('divisions')
            ->where('playoff','=',1)
            ->where('turnir_id','=',$turnirId)
            ->get()
            ->toArray();

        $groupA = [];
        $groupB = [];
        foreach ($teams as $team) {
            if ($team['divisions']['division'] == 'A') {
                $groupA[] = $team;
            } else {
                $groupB[] = $team;
            }
        }

        // Sorting teams by scores
        $divisionA = array_column($groupA,'divisions');
        $divisionB = array_column($groupB,'divisions');
        array_multisort(array_column($divisionA,'score'),SORT_DESC,$groupA);
        array_multisort(array_column($divisionB,'score'),SORT_DESC,$groupB);

        // Creating array rival teams
        $count = count($groupB)-1;
        $rivalTeams = [];
        foreach ($groupA as $group) {
            $rivalTeams[] = [
                'team_1' => $group['divisions']['teams_id'],
                'team_2' => $groupB[$count]['divisions']['teams_id'],
            ];
            $count--;
        }

        // Playing games
        $getRivals = function ($winners) {
            $rival = [];
            foreach ($winners as $key => $winner) {
                if ($key % 2 == 0) {
                    $rival[$key]['team_1'] = $winner;
                } else {
                    $rival[$key - 1]['team_2'] = $winner;
                }
            }
            return $rival;
        };
        $teamsController = new TeamsController();

        // Генерируем игры для каждого этапа
        $semi_final = $this->playGame($rivalTeams,$turnirId);
        $teamsController->setPlayOffTeams($semi_final['winners'],'semi_final');

        $final = $this->playGame($getRivals($semi_final['winners']),$turnirId);
        $teamsController->setPlayOffTeams($final['winners'],'final');
        $teamsController->setPlayOffTeams($final['losers'],'losers');

        $finalTeam = $this->playGame($getRivals($final['winners']),$turnirId)['winners'];
        $loserTeam = $this->playGame($getRivals($final['losers']),$turnirId)['winners'];

        $playOff->setPlayOffPlace($finalTeam,1,$turnirId);
        $playOff->setPlayOffPlace(array_diff($final['winners'],$finalTeam),2,$turnirId);
        $playOff->setPlayOffPlace($loserTeam,3,$turnirId);
        $playOff->setPlayOffPlace(array_diff($final['losers'],$loserTeam),4,$turnirId);


        return $playOff->getPlayOffGrid($turnirId);
    }

    /**
     * Генерация игр для плей-оффа и определения победителя или проигравшего
     * @param $rivalTeams
     * @return array
     */
    private function playGame($rivalTeams,$turnirId)
    {
        $result = [];
        $games = [];
        $rawWinners = [];
        foreach ($rivalTeams as $teams) {
            $teamGame1 = rand(0,7);
            $teamGame2 = rand(0,7);

            $game = [
                'team_1' => $teams['team_1'],
                'team_2' => $teams['team_2'],
                'score_1' => $teamGame1,
                'score_2' => $teamGame2,
                'turnir_id' => $turnirId,
                'type' => 'playoff'
            ];

            $gameResult = $this->checkGameResult($game);

            $games[] = $gameResult['game'];

            $rawWinners[] = $gameResult['winner'];

        }
        \App\Models\Games::insert($games);

        // тут нужно выловить проигравших в полуфинале
        if (count($rawWinners) == 2) {
            $teams = array_merge(array_column($rivalTeams,'team_1'),array_column($rivalTeams,'team_2'));
            $result['losers'] = array_values(array_diff($teams,$rawWinners));
        }
        $result['winners'] = $rawWinners;

        return $result;
    }

    /**
     * Определяем победителя 1 игры
     * @param $game - array игры
     * @return array - с игрой и победителем
     */
    private function checkGameResult($game)
    {
        $result = [];
        switch ($game['score_1'] <=> $game['score_2']) {
            case 1:
                $result['winner'] = $game['team_1'];
                $result['game'] = $game;
                break;
            case 0:
                $result['winner'] = $game['team_1'];
                $game['score_1']++;
                $result['game'] = $game;
                break;
            case -1:
                $result['winner'] = $game['team_2'];
                $result['game'] = $game;
        }

        return $result;
    }

}
