<?php

namespace App\Http\Controllers;

use App\Models\Divisions;
use App\Models\Games;
use App\Models\Teams;
use Illuminate\Http\Request;
use Inertia\Inertia;

class DivisionsController extends Controller
{

    /**
     * Get active turnir Division with teams and games
     * @param $type - divisions group A or B
     *
     * return array
     */
    public function getDivisions($type,$turnirId)
    {
        $divisions = Divisions::with('Teams')
            ->where('turnir_id', '=', $turnirId)
            ->where('division', 'LIKE', $type)
            ->get()
            ->toArray();

        $teams_id = array_column($divisions,'teams_id');

        $games = Games::where('type','LIKE','division')
            ->where('turnir_id','=',$turnirId)
            ->where(function ($query) use ($teams_id) {
                $query->whereIn('team_1',$teams_id);
                $query->orWhereIn('team_2',$teams_id);
            })
            ->get()
            ->toArray();

        $result = [];
        foreach ($divisions as $key => $division) {
            foreach ($games as $game) {
                if ($division['teams_id'] == $game['team_1']) {
                    $division['game'][$game['team_2']] = $game['score_1'].':'.$game['score_2'];
                } else if ($division['teams_id'] == $game['team_2']) {
                    $division['game'][$game['team_1']] = $game['score_2'].':'.$game['score_1'];
                }
            }
            $result[$key] = $division;
        }

        return $result;
    }

    /**
     * Генерируем новый дивизион с новой командой
     * @param Request $request
     * @return array
     */
    public function createDivision(Request $request)
    {
        $state = [
            'division' => $request->type,
            'turnir_id' => $request->turnir_id
        ];

        Teams::factory()->count(8)
            ->state(['turnir_id' => $request->turnir_id])
            ->has(Divisions::factory()->count(1)->state($state))
            ->create();

        $games = new GamesController();
        $games->generationDivisionGames($request);

        return $this->getDivisions($request->type, $request->turnir_id);
    }


}
