<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeamsController extends Controller
{
    /**
     * Функция для записи побудителей в предыдущем этапе
     * @param $teams - array id команд для обновления
     * @param $type - string тип плей-оффа
     */
    public function setPlayOffTeams($teams,$type)
    {
        DB::table('teams')
            ->whereIn('id',$teams)
            ->update([$type => 1]);
    }
}
