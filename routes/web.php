<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [\App\Http\Controllers\TurnirController::class, 'show']);

Route::post('generateDivision',[\App\Http\Controllers\DivisionsController::class, 'createDivision']);
Route::post('generatePlayOffGames', [\App\Http\Controllers\GamesController::class, 'generationPlayOffGames']);
Route::post('generateTurnir', [\App\Http\Controllers\TurnirController::class, 'generateTurnir']);


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');
